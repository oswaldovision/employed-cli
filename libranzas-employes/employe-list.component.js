(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/employed/";

    var fetchEmployes = function ($http) {
        var req = {
            method: 'GET',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var employedUpdate = function ($http, employed) {
        var req = {
            method: 'POST',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: employed
        }

        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var employedDelete = function ($http, numDoc) {
        var req = {
            method: 'DELETE',
            url: urlLibranzas + numDoc,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }

        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http) {
        var model = this;
        model.employes = [];
        model.employed = {
            numDoc: "",
            firstName: "",
            lastName: "",
            typeDocument: "",
            position: ""
        };

        model.$onInit = function () {
            fetchEmployes($http).then(function (employes) {
                model.employes = employes;
            });
        }

        model.editEmployed = function (employed) {
            model.employed = employed;
        }

        model.updateEmployed = function () {
            employedUpdate($http, this.employed).then(function () {
                fetchEmployes($http).then(function (employes) {
                    model.employes = employes;
                });
                model.employed = {
                    numDoc: "",
                    firstName: "",
                    lastName: "",
                    typeDocument: "",
                    position: ""
                };
            });

        }

        model.removeEmployed = function (numDoc) {
            employedDelete($http, numDoc).then(function () {
                fetchEmployes($http).then(function (employes) {
                    model.employes = employes;
                });
            });

        }
    }

    module.component("employedList", {
        templateUrl: "libranzas-employes/employe-list.component.html",
        controllerAs: "model",
        controller: ['$http', controller]
    })

})()
