(function () {
    "use strict";

    var module = angular.module("libranzaApp", ["ngComponentRouter","angular-loading-bar","ui.utils.masks"]);

    module.value("$routerRootComponent", "employedApp");

    module.component("appAbout", {
        templateUrl : "libranzas-employes/employe-about.component.html"
    });
}());