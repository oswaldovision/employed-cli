(function(){
    "use strict";

    var module = angular.module("libranzaApp");

    module.component("employedApp",{
        templateUrl : "/libranzas-employes/employe-app.component.html",
        $routeConfig : [
            { path : "/list" , component : "employedList" , name :"List"},
            { path : "/about" , component : "appAbout", name : "About"},
            { path : "/new" , component : "employedNew" , name :"New"},
            { path : "/request" , component : "employedRequest" , name :"Request"},
            { path : "/**" , redirectTo : ["List"] }
        ]
    });

})()     