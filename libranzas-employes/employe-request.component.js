-(function(){
    "use strict";

    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/request";
    const urlCompanies = "http://localhost:3045/v1/company";
    const urlFinancial = "http://localhost:3045/v1/financial";
    const urlEmployed = "http://localhost:3045/v1/employed";

    var addRequest = function($http, model){
        var req = {
            method: 'PUT',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: {
                financialId: model.financialId,
                employedId: model.employedId,
                companyId : model.companyId,
                ammount: model.ammount,
                price: model.price,
                salesValue: model.salesValue
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var getCompanies = function($http){
        var req = {
            method: 'GET',
            url: urlCompanies,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var getFinancials = function($http){
        var req = {
            method: 'GET',
            url: urlFinancial,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var getEmployed = function($http, numDoc){
        var req = {
            method: 'GET',
            url: urlEmployed + "/"+ numDoc,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http, $timeout, $location) {
        var model = this;


        model.$onInit = function () {
            model.dateRequest = "";
            model.ownerId = "";
            model.financialId = "";
            model.employedId = "";
            model.ammount = "";
            model.price = "";
            model.companyId = "";
            model.salesvalue = "";
            model.message = "";
            model.showError = false;
            model.doFade = false;

            getCompanies($http).then(function (allCompanies) {
                model.companies = allCompanies;
            });
            getFinancials($http).then(function (allFinancials) {
                model.financials = allFinancials;
            });
        }

        model.create = function () {
            getEmployed($http, model.employedId).then(function (employed) {
                if (employed.length){
                    addRequest($http, model).then(function (result) {
                        //reset
                        model.showError = false;
                        model.doFade = false;

                        model.showError = true;
                        model.message = result;

                        $timeout(function () {
                            model.doFade = true;
                            model.cleanModel();
                            $location.path("/request");

                        }, 2500);

                    }).catch(function (error) {
                        model.message = error;
                    });
                }else {
                    model.showError = false;
                    model.doFade = false;

                    model.showError = true;
                    model.message = "Empleado no existe en el sistema !";
                    $timeout(function () {
                        model.doFade = true;
                        $location.path("/request");
                    }, 2500);
                }
            }).catch(function (error) {
                model.message = error;
            });

        }

        model.cleanModel = function () {
            model.dateRequest = "";
            model.ownerId = "";
            model.financialId = "";
            model.employedId = "";
            model.ammount = "";
            model.price = "";
            model.companyId = "";
            model.salesvalue = "";
            model.message = "";
            model.showError = false;
        }
    }

    module.component("employedRequest",{
        templateUrl : "/libranzas-employes/employe-request.component.html",
        controllerAs: "model",
        controller: ['$http', '$timeout', '$location', controller]
    });

})()  