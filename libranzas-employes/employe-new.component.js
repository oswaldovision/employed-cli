(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/employed";

    var addEmploye = function ($http, model) {
        var req = {
            method: 'PUT',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: {
                numDoc: model.numDoc,
                firstName: model.firstName,
                lastName: model.lastName,
                typeDocument: model.typeDocument,
                position: model.position
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http, $timeout, $location) {
        var model = this;
        model.numDoc = "";
        model.firstName = "";
        model.lastName = "";
        model.typeDocument = "";
        model.position = "";
        model.message = "";
        model.showError = false;
        model.doFade = false;

        model.create = function () {

            addEmploye($http, model).then(function (result) {
                console.log(result);
                //reset
                model.showError = false;
                model.doFade = false;

                model.showError = true;
                model.message = result;

                $timeout(function () {
                    model.doFade = true;
                    $location.path("/list");
                }, 2500);

            }).catch(function (error) {
                model.message = error;
            });
        }
    }

    module.component("employedNew", {
        templateUrl: "libranzas-employes/employe-new.component.html",
        controllerAs: "model",
        controller: ['$http', '$timeout', '$location', controller]
    })

})()
